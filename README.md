# fis-deploy-qt-qiniu


## 千图网内部七牛上传插件 
  > 基于[fis3-deploy-ftp-fifth](https://www.npmjs.com/package/fis3-deploy-ftp-fifth) 优化

### fis3使用方式

    deploy: [
      fis.plugin('encoding'),
      fis.plugin('qt-qiniu', {
          console: true,
          cache: true, // 是否开启上传列表缓存，开启后支持跳过未修改文件，默认：true
          remoteDir: '/', // 远程文件目录，注意！！！设置错误将导致文件被覆盖
          rootDir: '../', // 设置git的位置，当前工程目录的相对路径
          connect: {
              host: 'v0.ftp.upyun.com',
              accessKey:'******',
              secretKey:'******',
              Bucket:'****',
              port:'21'
          }
      })
    ]
   
  
