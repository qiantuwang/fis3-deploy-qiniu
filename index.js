const fs = require('fs');
const path = require('path');
const async = require('async');
const qiniu = require('qiniu');
const http = require('http');
const Duplex = require('stream').Duplex;
const mime = require('mime');

let mac = null;
let formUploader = null;
let putExtra = null;
//记录首次上传失败的文件
let errorList = [];

module.exports = (options, modified, total, callback) => {
    options.remoteDir = options.remoteDir || '/';

    let ftpCache = fileCache(options),
        files = modified.length == 0 ? total : modified,
        fileCount = files.length,
        uploadFiles = options.cache ? ftpCache.filter(files) : files,
        uploadTotal = uploadFiles.length,
        uploadCount = 0;
    userOptions = options;
    fis.time('\n compile cost');
    // 如果总需要上传文件为0 则直接结束
    if (uploadTotal == 0) {
        cb('没有变更文件, 请检查catch文件是否需要删除!');
        return;
    } else {
        cb('一共有'+ uploadTotal +'个文件需要上传!------------------------');
        /**
         * note：遍历可上传文件信息进行上传
         * */

        async.mapLimit(uploadFiles, 1, (file, uploadCallback) => {

            // 初始化mac
            mac = new qiniu.auth.digest.Mac(options.connect.accessKey, options.connect.secretKey);
            const config = new qiniu.conf.Config();
            config.zone = qiniu.zone.Zone_z0
            putExtra = new qiniu.form_up.PutExtra()
            formUploader = new qiniu.form_up.FormUploader(config)

            let key = file.getHashRelease();
            if (file.ext === '.css' || file.ext === '.js') {
                key = key.replace(file.basename, file.filename + '_' + file.getHash() + file.ext).substring('1');
            } else {
                key = path.join(options.remoteDir, file.subpath).replace(/\\/g, '/').substring('1');
            }
            /**
             * note:七牛配置项
             * */
            let qiniuOptions = {
                scope : options.connect.Bucket + ':' + key,
                detectMime : 1
            };
            let putPolicy = new qiniu.rs.PutPolicy(qiniuOptions);
            let uploadToken = putPolicy.uploadToken(mac);
            /**
             * note:buffer转Stream
             * */
            let Stream = new Duplex();
            Stream.push(new Buffer(file.getContent()));
            Stream.push(null);

            /**
             * note:七牛流上传
             * */
            formUploader.putStream(uploadToken, key, Stream, putExtra, function (respErr, respBody, respInfo) {
                uploadCount++;
                if (respErr) {
                    throw respErr;
                }
                if (respInfo.statusCode == 200) {
                    cg('\n -' + uploadCount + '--> ' + key + ' 上传成功!');
                    uploadCallback(null, file);
                } else {
                    console.log(respInfo)
                    errorList.push(file);
                    cg('\n -' + uploadCount + '--> ' + key + ' 上传失败!');
                    uploadCallback(null, file);
                }
            });

    },()=>{
            if(errorList.length > 0 ){
                uploadCount = 0;
                cb('一共有'+ errorList.length +'个文件需要重新上传!------------------------');
                reUploadQiniu(errorList,options,uploadCount);
            }
        });
    }
};

/**
 * 打包重试
 *
 * */
module.exports.reUploadQiniu = reUploadQiniu = (dataList,options,uploadCount) =>{
    async.mapLimit(dataList, 1, (file, uploadCallback) => {
        // 初始化mac
        mac = new qiniu.auth.digest.Mac(options.connect.accessKey, options.connect.secretKey);
        const config = new qiniu.conf.Config();
        config.zone = qiniu.zone.Zone_z0
        putExtra = new qiniu.form_up.PutExtra()
        formUploader = new qiniu.form_up.FormUploader(config)
        errorList = [];
        let key = file.getHashRelease();
        let qiniuOptions = null;
        if (file.ext === '.css' || file.ext === '.js') {
            key = key.replace(file.basename, file.filename + '_' + file.getHash() + file.ext).substring('1');
        } else {
            key = path.join(options.remoteDir, file.subpath).replace(/\\/g, '/').substring('1');
        }
        /**
         * note:七牛配置项
         * */
        qiniuOptions = {
            scope : options.connect.Bucket + ':' + key,
            detectMime : 1
        };
        let putPolicy = new qiniu.rs.PutPolicy(qiniuOptions);
        let uploadToken = putPolicy.uploadToken(mac);

        /**
         * note:buffer转Stream
         * */
        let Stream = new Duplex();
        Stream.push(new Buffer(file.getContent()));
        Stream.push(null);
        /**
         * note:七牛流上传
         * */
        formUploader.putStream(uploadToken, key, Stream, putExtra, function (respErr, respBody, respInfo) {
        uploadCount++;
        if (respErr) {
            throw respErr;
        }
        if (respInfo.statusCode == 200) {
            cg('\n -' + uploadCount + '--> ' + key + ' 重试上传成功!');
            uploadCallback(null, file);
        } else {
            errorList.push(file);
            cg('\n -' + uploadCount + '--> ' + key + ' 重试上传失败!');
            uploadCallback(null, file);
        }
    });

}, () => {
        cg('\n ------------------------------------------------------------->')
        if (errorList.length > 0){
            cg(dataList.length - errorList.length +'个文件重新打包打包上传成功!');
            cg(errorList.length +'个文件打包重新上传失败, 请手动重新打包');
        } else {
            cg(dataList.length +'个文件重新打包上传成功!');
        }
    });
}

/**
 * 缓存文件信息
 *
 */
module.exports.fileCache = fileCache = (opts) => {
    let gitHEAD = fs.readFileSync(path.join(fis.project.getProjectPath(), opts.rootDir, '.git/HEAD'), 'utf-8').trim() // ref: refs/heads/develop
    let ref = gitHEAD.split(': ')[1] // refs/heads/develop
    let gitBranchName = gitHEAD.split('/')[2] // 环境：eg:develop

    let tmpPath = fis.project.getProjectPath() + path.sep + 'fis3_deploy_ftp' + path.sep + parsePath(opts.connect.host + ':' + opts.connect.port),
        jsonPath = tmpPath + path.sep + parsePath(opts.remoteDir) + gitBranchName + '_sourcemap.json',
        defaultPath = tmpPath + path.sep + parsePath(opts.remoteDir) + 'master_sourcemap.json';

    // fis.log.debug('tmpPath: %s', jsonPath);

    let cache = {};
    if (fis.util.isFile(jsonPath)) {
        if (opts.cache) {
            cache = fis.util.readJSON(jsonPath);
        } else {
            fis.util.del(jsonPath);
        }
    } else if (fis.util.isFile(defaultPath)) {
        if (opts.cache) {
            cache = fis.util.readJSON(defaultPath);
        }
    }

    function filter(files) {
        let result = [];
        files.forEach(function (file) {
            let id = file.getId(),
                hash = file.getHash();

            // fis.log.debug('%s : %s', id, hash);
            if (!cache[id] || cache[id] != hash) {
                cache[id] = hash;
                result.push(file);
            }
        });

        if (result.length > 0) save();

        return result;
    }

    function parsePath(path) {
        if (!path) return '';
        return path.replace(/^\/+/, '').replace(/\/\/(.*):(.*)@/, '').replace(/[:\/\\\.-]+/g, '_');
    }

    function save() {
        fis.util.write(jsonPath, JSON.stringify(cache,null,4));
    }

    return {
        filter: filter
    };
}

/***
 * 遍历文件信息
 *
 */
module.exports.resolveDir = resolveDir = (dirname, cb, dest) => {
    if (remoteDirCache[dirname]) {
        cb(false, remoteDirCache[dirname]);
        return;
    }

    let listRemote = function () {
        let queues = resolveing[dirname] || (resolveing[dirname] = []);
        if (queues.length) {
            queues.push(cb);
        } else {
            queues.push(cb);

            let listFileCallback = function (err, list) {
                if (err) {
                    ftpQueue && ftpQueue.destroy();
                    ftpQueue = createFtpQueue(userOptions);
                    ftpQueue.listFiles(dirname, listFileCallback);
                    return;
                }

                let fn = function () {
                    remoteDirCache[dirname] = true;
                    delete resolveing[dirname];
                    queues.forEach(function (cb) {
                        cb(list);
                    });
                };

                if (!list || list.length == 0) {
                    ftpQueue.addDir(dirname, fn);
                } else {
                    fn();
                }
            };

            ftpQueue.listFiles(dirname, listFileCallback);
        }
    }

    if (~dirname.indexOf(path.sep) && path.dirname(dirname) !== dirname) {
        resolveDir(path.dirname(dirname), listRemote, dest);
    } else {
        listRemote();
    }
}

/**
 * 配置信息
 *
 */
module.exports.options = {
    console: true,
    cache: true, // 是否开启上传列表缓存，开启后支持跳过未修改文件，默认：true
    remoteDir: '/', // 远程文件目录，注意！！！设置错误将导致文件被覆盖
    rootDir: '../', // 设置git的位置，当前工程目录的相对路径
    connect: {
        host: '*******',
        accessKey:'*******',
        secretKey:'*******',
        Bucket:'*******',
        port:'21'
    }
};

/**
 * 输出信息到控制台
 *
 */
module.exports.cb = cb = (info) => {
    process.stdout.write(
        '\n qiniu:'.green.bold + info
    );
}

/**
 * 控制台输出 console
 * @param {String} info
 */
const cg = (info) => {
    console.log(info);
}

